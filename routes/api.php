<?php

use App\Http\Controllers\ApiAuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::apiResource('users', UserController::class)
    ->missing(function (Request $request) {
        return Redirect::route('users.index');
    });;


Route::post('auth/sign-up', [ApiAuthController::class, 'signUp']);
Route::post('auth/sign-in', [ApiAuthController::class, 'signIn']);

Route::middleware('auth:sanctum')->get('auth/sign-out', [ApiAuthController::class, 'signOut']);

Route::middleware('auth:sanctum')->get('auth/user', function (Request $request) {
    return $request->user();
});

