<?php

use App\Http\Controllers\AdController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ShortLinkController;
use App\Http\Middleware\EnsureLinkIdExists;
use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// index
Route::get('/', [HomeController::class, 'index'])->name('home');

// login, logout
Route::get('/login', [LoginController::class, 'login'])->
        middleware('guest')->name('login');

Route::post('/authenticate', [LoginController::class, 'authenticate'])->
        middleware('guest')->name('authenticate');

Route::get('/logout', [LogoutController::class, 'logout'])->
        middleware('auth')->name('logout');

// link's CRUD
Route::resource('links', LinkController::class)
    ->middleware('auth');

Route::get('/{uuid}', [ShortLinkController::class, 'find']);


// fallback route
Route::fallback(function () {
    return redirect('/');
});
