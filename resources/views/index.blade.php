@extends('layout')

@section('content')

    <p>Link's list</p>

    <div class="row mb-3">
        {{ $links->links() }}
    </div>

    <div class="row">

        <div class="col">
            <a href="{{ route('links.create') }}" class="btn btn-success">New link</a>
        </div>

        <table class="table table-striped">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Full link</th>
                    <th scope="col">Short link</th>
                    <th scope="col">Clicks count</th>
                    <th scope="col">Manage</th>
                </tr>
            </thead>
            <tbody>

                    @forelse ($links as $key => $link)
                        <tr>
                            <th scope="row">{{ $key }}</th>
                            <td>{{ $link->link }}</td>
                            <td>{{ url('/' . $link->uuid) }}</td>
                            <td>{{ $link->clicks }}</td>
                            <td><a href="{{ route('links.show', ['link'=> $link->id]) }}" class="btn btn-info">Детали</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">No links</td>
                        </tr>
                    @endforelse

            </tbody>
        </table>
    </div>

    <div class="row mb-3">
        {{ $links->links() }}
    </div>

@endsection
