@extends('layout')

@section('content')

    <p>Link</p>

    <div class="row">

            <div class="col">
                <div class="card">
                    <div class="card-body">

                            <h5 class="card-title">Link id: {{ $link->id }}</h5>
                            <h5 class="card-text">Original link: {{ $link->link }}</h5>
                            <h5 class="card-text">Short link: {{ url('/' . $link->uuid) }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Clicks count: {{ $link->clicks }}</h6>

                        @can('update', $link)
                        <a href="{{ route('links.edit', ['link' => $link->id]) }}" class="btn btn-success">Редактировать</a>
                        @endcan
                        @can('delete', $link)
                            <form action="{{ route('links.destroy', ['link' => $link->id]) }}" method="post">
                                @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button type="submit" class="btn btn-danger">Удалить</button>
                            </form>
                        @endcan
                    </div>
                </div>
            </div>

    </div>

    <hr>
    <div class="row">
        <p>Статистика посещений</p>
        <div class="row mb-3">
            {{ $statistics->links() }}
        </div>

        <table class="table table-striped">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">ip</th>
                <th scope="col">Country_code</th>
                <th scope="col">Country name</th>
                <th scope="col">City name</th>
                <th scope="col">Date</th>
            </tr>
            </thead>
            <tbody>
        @forelse ($statistics as $key => $statistic)
            <tr>
                <th scope="row">{{ $key }}</th>
                <td>{{ $statistic->ip }}</td>
                <td>{{ $statistic->country_code }}</td>
                <td>{{ $statistic->country_name }}</td>
                <td>{{ $statistic->city_name }}</td>
                <td>{{ $statistic->updated_at }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6">No statistics</td>
            </tr>
        @endforelse
            </tbody>
        </table>

        <div class="row mb-3">
            {{ $statistics->links() }}
        </div>
    </div>

@endsection
