@extends('layout')

@section('content')

    <p>Link form. {{ $description }}</p>

    <div class="row">

        <form action="{{ $route ?? route('links.store') }}" method="post">
            @csrf

            @if(!empty($method))
                <input name="_method" type="hidden" value="{{ $method }}">
            @endif

            @error('link')
            <hr>
            <div class="alert alert-danger"> {{ $message }} </div>
            @enderror
            <div class="mb-3">
                <label for="link" class="form-label">Link</label>
                <input type="url" class="form-control" id="link"
                       name="link" placeholder="http://example.com"
                       value="{{ old('link') ?? $link->link ?? '' }}">
            </div>

            <button type="submit" class="btn btn-success">{{ $button ?? 'Создать'}}</button>
        </form>
    </div>

@endsection
