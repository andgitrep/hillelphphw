<?php

namespace App\Jobs;

use App\Models\Link;
use App\Services\IpParser\Repositories\IpParserRepository;
use GeoIp2\Database\Reader;
use Gruzdev\IpParser\Adapters\Interfaces\ParserAdapterInterface;
use Gruzdev\IpParser\Adapters\MaxMindAdapter;
use Gruzdev\IpParser\Interfaces\IpParserServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class ParseIpJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Link
     */
    private Link $link;
    /**
     * @var string
     */
    private string $ip;

    /**
     * Create a new job instance.
     *
     * @param Link $link
     * @param string $ip
     */
    public function __construct(Link $link, string $ip)
    {
        $this->link = $link;
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @param IpParserServiceInterface $ipParserService
     * @return void
     * @throws \Exception
     */
    public function handle(IpParserServiceInterface $ipParserService)
    {
        $data = [];
        $data['ip'] = $this->ip;

        if ($ipParserService->parse($this->ip)) {
            $data['country_code'] = $ipParserService->getCountryCode();
            $data['country_name'] = $ipParserService->getCountryName();
            $data['city_name'] = $ipParserService->getCityName();
        } else {
            $data['country_code'] = 0;
            $data['country_name'] = 'unknown';
            $data['city_name'] = 'unknown';
        }

        (new IpParserRepository())->save($data, $this->link);
    }
}
