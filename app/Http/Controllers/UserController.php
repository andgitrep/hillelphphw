<?php

namespace App\Http\Controllers;


use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return UserCollection
     */
    public function index()
    {
        return new UserCollection(User::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors()
            ],
                422);
        }

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $user->save();

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if (strtolower($request->method()) === 'put') {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users|max:255',
                'password' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "errors" => $validator->errors()
                ],
                    422);
            }

            $user->name = $request->name;
            $user->email = $request->email;
            $user->email_verified_at = now();
            $user->password = Hash::make($request->password);

        } elseif (strtolower($request->method()) === 'patch') {
            $validator = Validator::make($request->all(), [
                'password' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "errors" => $validator->errors()
                ],
                    422);
            }
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return UserResource
     */
    public function destroy(User $user)
    {
        $user->delete();

        return new UserResource($user);
    }
}
