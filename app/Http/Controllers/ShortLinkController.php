<?php


namespace App\Http\Controllers;


use App\Jobs\ParseIpJob;
use App\Models\Link;
use Illuminate\Http\Request;

class ShortLinkController extends Controller
{

    /**
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function find(Request $request, $uuid)
    {

        $link = Link::where('uuid', '=', $uuid)->firstOr(function () {
            return false;
        });

        if(!$link) {
            return abort(404);
        }

        ParseIpJob::dispatch($link, $request->ip());

        return redirect()->to($link->link);
    }


}
