<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Services\IpParser\IpParserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->view('index', [
            'links' => $request->user()->links()->paginate(50),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param IpParserService $ipParserService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        return view('linkForm', [
            'description' => 'Введите URL для генерации короткого URL'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'link' => 'required|url',
        ]);

        if ($validator->fails()) {
            return redirect()->route('links.create', [
                'description' => 'Введите URL для генерации короткого URL'
            ])
                ->withErrors($validator)
                ->withInput();
        }

        $uniqid = uniqid();
        while(true) {
            if (!(Link::where('uuid', '=', $uniqid)->first())) {
                break;
            }
            $uniqid = uniqid();
        }

        $link = new Link();

        $link->fill([
            'user_id' => Auth::user()->id,
            'uuid' => $uniqid,
            'link' => $request->input('link'),
            'clicks' => 0,
        ]);

        $link->save();

        return redirect()->route('links.show', ['link'=> $link->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Link $link)
    {
        return view('linkView', [
           'link' => $link,
            'statistics' => $link->statistics()->paginate(10),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Link $link)
    {
        $this->middleware('id.policy.check');

        return view('linkForm', [
            'link' => $link,
            'description' => 'Тут можно изменить url не меняя ее коротокой ссылки',
            'method' => 'PUT',
            'button' => 'Обновить',
            'route' => route('links.update', ['link' => $link->id]),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Link $link)
    {
        $this->middleware('id.policy.check');

        $validator = Validator::make($request->all(), [
            'link' => 'required|url',
        ]);

        if ($validator->fails()) {
            return redirect()->route('links.edit', ['link' => $link->id])
                ->withErrors($validator)
                ->withInput();
        }

        $link->fill([
            'link' => $request->input('link'),
        ]);

        $link->save();

        return redirect()->route('links.show', ['link'=> $link->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Link $link)
    {
        $this->middleware('id.policy.check');

        $link->delete();

        return redirect()->route('links.index');
    }
}
