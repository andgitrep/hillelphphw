<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        return redirect()->route('links.index');
    }

}
