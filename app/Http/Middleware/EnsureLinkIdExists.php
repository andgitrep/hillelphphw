<?php

namespace App\Http\Middleware;

use App\Models\Ad;
use App\Models\Link;
use Closure;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class EnsureLinkIdExists
{

    use AuthorizesRequests;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function handle(Request $request, Closure $next)
    {
        $link = Link::find($request->link);

        if (!$link) {
            return redirect()->route('links.index')->withErrors([
                'flash' => 'Can\'t find link'
            ]);
        }

        $this->authorize('update', $link);
        $this->authorize('delete', $link);

        return $next($request);
    }
}
