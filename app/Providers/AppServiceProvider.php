<?php

namespace App\Providers;

use App\Services\IpParser\Repositories\Interfaces\IpParserRepositoryInterface;
use App\Services\IpParser\Repositories\IpParserRepository;
use GeoIp2\Database\Reader;
use GeoIp2\ProviderInterface;
use Gruzdev\IpParser\Adapters\Interfaces\ParserAdapterInterface;
use Gruzdev\IpParser\Adapters\IpApiAdapter;
use Gruzdev\IpParser\Adapters\MaxMindAdapter;
use Gruzdev\IpParser\Interfaces\IpParserServiceInterface;
use Gruzdev\IpParser\IpParserService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public $bindings = [
//        ParserAdapterInterface::class => IpApiAdapter::class,
                ParserAdapterInterface::class => MaxMindAdapter::class,
        IpParserServiceInterface::class => IpParserService::class,
        IpParserRepositoryInterface::class => IpParserRepository::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
    }
}
