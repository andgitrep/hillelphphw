<?php


namespace App\Services\Singleton;


class AdminEmailSingleton
{
    /**
     * @var null
     */
    private static $_object = null;

    /**
     * @var string
     */
    private string $email = "admin@test.com";

    /**
     * @inheritDoc
     */
    public static function getInstance()
    {
        if (!self::$_object) {
            self::$_object = new self();
        }

        return self::$_object;
    }

    /**
     * AdminEmailSingleton constructor.
     */
    private function __construct() { }

    /**
     *
     */
    protected function __clone() { }

    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
