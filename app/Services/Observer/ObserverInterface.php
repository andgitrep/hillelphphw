<?php


namespace App\Services\Observer;


interface ObserverInterface
{
    /**
     * @param SubscriberInterface $subscriber
     * @return mixed
     */
    public function addSubscriber(SubscriberInterface $subscriber);

    /**
     * @param ListenerDataProvider $dataProvider
     * @return mixed
     */
    public function notifySubscriber(ListenerDataProvider $dataProvider);
}
