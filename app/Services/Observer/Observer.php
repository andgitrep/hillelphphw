<?php


namespace App\Services\Observer;


class Observer implements ObserverInterface
{

    /**
     * @var SubscriberInterface[]
     */
    private $subscribers = [];

    /**
     * @param SubscriberInterface $subscriber
     */
    public function addSubscriber(SubscriberInterface $subscriber)
    {
        $this->subscribers[] = $subscriber;
    }

    /**
     * @param ListenerDataProvider $dataProvider
     */
    public function notifySubscriber(ListenerDataProvider $dataProvider)
    {
        foreach ($this->subscribers as $subscriber) {
            $subscriber->handle($dataProvider);
        }
    }
}
