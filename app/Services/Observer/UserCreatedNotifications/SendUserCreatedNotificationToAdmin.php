<?php


namespace App\Services\Observer\UserCreatedNotifications;


use App\Services\Observer\ListenerDataProvider;
use App\Services\Observer\SubscriberInterface;
use App\Services\Singleton\AdminEmailSingleton;

class SendUserCreatedNotificationToAdmin implements SubscriberInterface
{

    private string $email;

    public function __construct()
    {
        $this->email = AdminEmailSingleton::getInstance()->getEmail();
    }

    public function handle(ListenerDataProvider $dataProvider)
    {
        echo __CLASS__ . ";<br> data = $dataProvider; subscriberEmail = " . $this->email . '<br>';
    }
}
