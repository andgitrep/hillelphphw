<?php


namespace App\Services\Observer\UserCreatedNotifications;


use App\Services\Observer\ListenerDataProvider;
use App\Services\Observer\SubscriberInterface;
use App\Services\Singleton\ManagerEmailSingleton;

class SendUserCreatedNotificationToManager implements SubscriberInterface
{

    private string $email;

    public function __construct()
    {
        $this->email = ManagerEmailSingleton::getInstance()->getEmail();
    }

    public function handle(ListenerDataProvider $dataProvider)
    {
        echo __CLASS__ . ";<br> data = $dataProvider; subscriberEmail = " . $this->email . '<br>';
    }
}
