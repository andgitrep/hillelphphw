<?php


namespace App\Services\Observer\UserCreatedNotifications;


use App\Services\Observer\ListenerDataProvider;
use App\Services\Observer\SubscriberInterface;
use App\Services\Multiton\UserEmailMultiton;

class SendUserCreatedNotificationToUser implements SubscriberInterface
{

    private string $email;

    public function __construct(string $userName)
    {
        $this->email = UserEmailMultiton::getInstance($userName)->getEmail();
    }

    public function handle(ListenerDataProvider $dataProvider)
    {
        echo __CLASS__ . ";<br> data = $dataProvider; subscriberEmail = " . $this->email . '<br>';
    }
}
