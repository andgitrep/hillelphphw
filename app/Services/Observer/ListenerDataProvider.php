<?php


namespace App\Services\Observer;


class ListenerDataProvider
{

    /**
     * @var array
     */
    private array $data;

    /**
     * ListenerDataProvider constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {

        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return print_r($this->data, true);
    }

}
