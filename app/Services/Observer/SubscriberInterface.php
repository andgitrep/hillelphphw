<?php


namespace App\Services\Observer;


interface SubscriberInterface
{
    /**
     * @param ListenerDataProvider $dataProvider
     * @return mixed
     */
    public function handle(ListenerDataProvider $dataProvider);
}
