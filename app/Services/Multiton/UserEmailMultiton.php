<?php


namespace App\Services\Multiton;


class UserEmailMultiton
{


    /**
     * @var UserEmailMultiton[]
     */
    private static array $emails = [];

    /**
     * @var string
     */
    private string $email = '';
    /**
     * @inheritDoc
     */
    public static function getInstance(string $userName)
    {
        if (!array_key_exists($userName, self::$emails)) {
            self::$emails[$userName] = new UserEmailMultiton();
        }

        return self::$emails[$userName];
    }

    /**
     * UserEmailMultiton constructor.
     */
    private function __construct() {
        $this->email = array_key_last(self::$emails) . "user@test.com";
    }

    /**
     *
     */
    protected function __clone() { }

    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a multiton.");
    }

    /**
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }
}
