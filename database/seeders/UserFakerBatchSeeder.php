<?php

namespace Database\Seeders;

use App\Models\Link;
use App\Models\Statistic;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\DB;

class UserFakerBatchSeeder extends Seeder
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clicks = 1000;
        $linksCount = 100;
        $usersCount = 2;

        $users = User::factory($usersCount)->create()->each(function ($user) use ($clicks, $linksCount, $usersCount) {
            echo 'user ' . $user->id . " of $usersCount \n";
            $timeStart = microtime(true);
            $links = Link::factory($linksCount)->make([
                'user_id' => $user->id,
                'clicks' => $clicks
            ])->toArray();

            $dispatcher = DB::getEventDispatcher();

            // disabling the events
            DB::unsetEventDispatcher();

            DB::table('links')->insert($links);

            // enabling the event dispatcher
            DB::setEventDispatcher($dispatcher);

            $time = microtime(true) - $timeStart;
            echo 'link create time = ' . $time . " s\n";

            $statistics = [];
            $chunk = 1;
            $chunkCount = 10;
            Link::select('id')->where('user_id', '=', $user->id)
                ->chunk($chunkCount, function ($links) use (&$chunk, $clicks, $chunkCount){
                    echo "\t links chunk " . $chunk . " of $chunkCount\n";
                    echo "\t- max memory" . ini_get('memory_limit') . "\n";

                    echo "\t- statistics factory create \n";
                    $timeStart = microtime(true);
                    $links->each(function ($link) use ($clicks) {
                        echo "\t link id $link->id; memory " . memory_get_peak_usage(true) / 1024 /1024 . "Mbytes\n";

                        $statistics = Statistic::factory($clicks)->make([
                            'link_id' => $link->id
                        ])->toArray();

                        $dispatcher = DB::getEventDispatcher();

                        // disabling the events
                        DB::unsetEventDispatcher();

                        DB::table('statistics')->insert($statistics);

                        // enabling the event dispatcher
                        DB::setEventDispatcher($dispatcher);

                    });
                    echo "\t- statistics create time = " . (microtime(true) - $timeStart) . " s\n";
                    $chunk++;

                });
        });
    }
}
