<?php

namespace Database\Seeders;

use App\Models\Link;
use App\Models\Statistic;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Generator;

class UserEloquentSeeder extends Seeder
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clicks = 100;
        $linksCount = 10;

        $users = User::factory(2)->create()->each(function ($user) use ($clicks, $linksCount) {
            $user->links()->saveMany(Link::factory($linksCount)->make([
                'user_id' => $user->id,
                'clicks' => $clicks
            ]))->each(function ($link) use ($clicks) {
                $link->statistics()->saveMany(Statistic::factory($clicks)->make([
                    'link_id' => $link->id
                ]));
            });
        });
    }
}
