<?php

namespace Database\Seeders;

use App\Models\Link;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\DB;

class FastUserBatchSeeder extends Seeder
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clicks = 1000;
        $linksCount = 100;
        $usersCount = 2;

        $users = User::factory($usersCount)->create()->each(function ($user) use ($clicks, $linksCount, $usersCount) {
            echo 'user ' . $user->id . " of $usersCount \n";
            $timeStart = microtime(true);
            $links = [];
            for ($i=0; $i<$linksCount; $i++) {
                $links[] = [
                    'user_id' => $user->id,
                    'uuid' => uniqid(),
                    'link' => $this->faker->url,
                    'clicks' => $clicks,
                ];

            }
            Link::insert($links);
            echo 'link create time = ' . (microtime(true) - $timeStart) . " s\n";

            $statistics = [];
            $chunk = 1;
            $chunkCount = 10;
            Link::select('id')->where('user_id', '=', $user->id)
                ->chunk($chunkCount, function ($links) use (&$chunk, $clicks, $chunkCount){
                    echo "\tchunk " . $chunk . " of $chunkCount\n";
                    echo "\t- max memory" . ini_get('memory_limit') . "\n";

                    $timeStart = microtime(true);
                    foreach ($links as $key=>$link){
                        echo "\t link id $link->id; memory " . memory_get_peak_usage(true) / 1024 /1024 . "Mbytes\n";
                        for ($i=0; $i<$clicks; $i++) {
                            $statistics[] = [
                                'link_id' => $link->id,
                                'ip' => $this->faker->ipv4,
                                'country_code' => $this->faker->countryCode,
                                'country_name' => $this->faker->country,
                                'city_name' => $this->faker->city,
                            ];
                        }
                    }
                    echo "\t- save chunk " . $chunk++ . "\n";
                    $dispatcher = DB::getEventDispatcher();

                    // disabling the events
                    DB::unsetEventDispatcher();

                    DB::table('statistics')->insert($statistics);

                    // enabling the event dispatcher
                    DB::setEventDispatcher($dispatcher);
                    echo "\t- statistics create time = " . (microtime(true) - $timeStart) . " s\n";

                    $statistics = [];
                });
        });
    }
}
